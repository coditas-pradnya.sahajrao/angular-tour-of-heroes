import { Component, OnInit } from '@angular/core';
import { HEROES } from 'src/app/mock-heroes';
import { Hero } from '../../Interface/Hero';
import { HeroService } from 'src/app/hero.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {

  heroes: Hero[] = [];
  selectedHero?: Hero;

  constructor(private heroService: HeroService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getHeroes()
  }

  onSelect(hero: Hero): void {
    this.messageService.add(`You selected Hero with id of${hero.id} and name${hero.name}`)
    console.log(hero);
    this.selectedHero = hero;
  }

  getHeroes(): void {
   this.heroService.getHeroes().subscribe(x=>{
    console.log(x) 
    this.heroes = x;
   })
  }
}
